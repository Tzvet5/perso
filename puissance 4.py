# Puissance 4 ; Projet de NSI

from tkinter import *
from PIL import Image, ImageTk
from functools import partial
import time

# Paramétrage de l'interface graphique :
dim=50 # Dimension en pixels d'une image
rayon = dim//2
a,b=dim//2,dim//2
R,V,B,A = 0,0,0,0 # Rouge, vert, bleu, alpha (transparence)
# Trois images transparentes au départ (canal alpha = 0) définies avec PIL
img_jaune=Image.new ('RGBA',(dim,dim),(R,V,B,A))
img_rouge=Image.new ('RGBA',(dim,dim),(R,V,B,A))
img_blanc=Image.new ('RGBA',(dim,dim),(R,V,B,A))

# Éléments de l'interface graphique :
for x in range(0,dim):
    for y in range(0,dim):
        if (x-a)**2+(y-b)**2<rayon**2: # Dessine un disque de centre (a,b)
            img_jaune.putpixel((x,y),(255,255,000,255))
            img_rouge.putpixel((x,y),(255,000,000,255))
            img_blanc.putpixel((x,y),(255,255,255,255))



def jouer(j):
    global tour1,jouer_encours,adjacences_1,adjacences_2

    # Sécurité pour ne permettre qu'un seul tour à la fois :
    if jouer_encours:return
    jouer_encours=True
    # Animation de descente du pion :
    i=0
    while i+1<=n and pion[i][j]==0:
        b=boutons[i][j]
        if tour1:
            b.config(image=img_rouge_tk)
        else:
            b.config(image=img_jaune_tk)
        ecran.update()
        time.sleep(0.1)
        i=i+1
        if i-1>=0 and i<n and pion[i][j]==0:
            b2=boutons[i-1][j]
            b2.config(image=img_blanc_tk)
    # Placement du pion :
    if tour1:
        pion[i-1][j]=1
        update_adjacences(adjacences_1,i-1,j)
        if check_victoire(adjacences_1):
            print("Le joueur Rouge a gagné !")
            return
    else:
        pion[i-1][j]=2
        update_adjacences(adjacences_2,i-1,j)
        if check_victoire(adjacences_2):
            print("Le joueur Jaune a gagné !")
            return
    # Désactivation de la sécurité ; Tour suivant :
    tour1=not tour1
    jouer_encours=False

def update_adjacences(adjacences,i,j):
    # Détection des listes d'adjacences adjacentes à mette à jour :
    l_updates=[]
    for l in adjacences:
        for e in [(-1,-1),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]:
            i_check=i ; j_check=j
            if i_check+e[0]>=0 and i_check+e[0]<n and j_check+e[1]>=0 and j_check+e[1]<p:
                i_check+=e[0]
                j_check+=e[1]
            if (i_check,j_check) in l and pion[i_check][j_check]==pion[i][j] and not l in l_updates:
                l_updates.append(l)
    # Mise à jour des listes d'adjacences :
    if len(l_updates)>1: # Fuisonner les listes d'adjacences
        merged_list=[]
        while len(l_updates)>0:
            adjacences.remove(l_updates[0])
            for e in l_updates.pop(0):
                merged_list.append(e)
        adjacences.append(merged_list)
    elif len(l_updates)==1: # Allonger la liste d'adjacences
        adjacences[adjacences.index(l_updates[0])].append((i,j))
    else: # Créer une liste d'adjacences
        adjacences.append([(i,j)])

def check_victoire(adjacences):
    for l in adjacences:
        for e in l:
            print(adjacences)
            if ((e[0]+1,e[1]) in l and (e[0]+2,e[1]) in l and (e[0]+3,e[1]) in l) or ((e[0]-1,e[1]) in l and (e[0]-2,e[1]) in l and (e[0]-3,e[1]) in l) or ((e[0],e[1]+1) in l and (e[0],e[1]+2) in l and (e[0],e[1]+3) in l) or ((e[0],e[1]-1) in l and (e[0],e[1]-2) in l and (e[0],e[1]-3) in l) or ((e[0]+1,e[1]+1) in l and (e[0]+2,e[1]+2) in l and (e[0]+3,e[1]+3) in l) or ((e[0]+1,e[1]-1) in l and (e[0]+2,e[1]-2) in l and (e[0]+3,e[1]-3) in l) or ((e[0]-1,e[1]+1) in l and (e[0]-2,e[1]+2) in l and (e[0]-3,e[1]+3) in l) or ((e[0]-1,e[1]-1) in l and (e[0]-2,e[1]-2) in l and (e[0]-3,e[1]-3) in l):
                return True
    return False



# Initialisation :
jouer_encours=False
tour1=True
n=6 # Nombre de lignes
p=7 # Nombres de colonnes
boutons=[[None for _ in range(p)] for _ in range(n)] # tableau de bouts
pion=[[0 for _ in range(p)] for _ in range(n)] # 0 aucune couleur, 1 rouge, 2 jaune, i = lignes, j = colonnes
adjacences_1=[]
adjacences_2=[]



# Interface graphique :
ecran=Tk()
# Les trois images PIL sont convertis en image tkinter
img_jaune_tk=ImageTk.PhotoImage(img_jaune)
img_rouge_tk=ImageTk.PhotoImage(img_rouge)
img_blanc_tk=ImageTk.PhotoImage(img_blanc)
for i in range(n):
    for j in range(p):
        b=Button(ecran,bg='blue') # Fond bleu
        b['command']= partial(jouer,j)
        b['image']=img_blanc_tk
        b.grid(row=i,column=j) # Les boutons sont placés en formant une grille
        boutons[i][j]=b

ecran.mainloop()